//
//  CollectionViewCell.swift
//  AssignmentApp
//
//  Created by Zolo on 27/01/23.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
}
