//
//  ViewController.swift
//  AssignmentApp
//
//  Created by Zolo on 27/01/23.
//

import UIKit


class TableHeaderView : UITableViewHeaderFooterView{
    
    
    
    
    
    let identifier = "TableHeader"
    private let label : UILabel = {
        let label = UILabel()
        label.text = "Comics"
        label.textAlignment = .center
        
        return label
        
    }()
    
    override init(reuseIdentifier : String?){
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.addSubview(label)
        
        

    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        label.sizeToFit()
        
    }
    
}

var data = [
    
    Comics(category: "Batman", comics: ["batman","batman2","batman3","batman4"]),
    Comics(category: "Superman", comics: ["superman1","superman2","superman3","superman4"]),
    Comics(category: "Spiderman", comics: ["spiderman1","spiderman2","spiderman3","spiderman4"])
    
]

class ViewController: UIViewController {
    
    

    @IBOutlet weak var tableView: UITableView!
    
    
    
    func showAlert(){
        
        let alert = UIAlertController(title: "Title", message: "hello", preferredStyle:  .alert)
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
            
            print("tapped cancel")
            
        }))
        
        present(alert, animated: true)
        
    }
    func showActionSheet(){
        
    }

    
    
    @IBAction func addButton(_ sender: Any) {
        showAlert()
    }
    
    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }


}



extension ViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : TableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.collectionView.tag = indexPath.section
         
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].category
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        view.tintColor = .gray.withAlphaComponent(0.9)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
        
    }
    
}


