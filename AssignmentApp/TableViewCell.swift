//
//  TableViewCell.swift
//  AssignmentApp
//
//  Created by Zolo on 27/01/23.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    

    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension TableViewCell : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data[collectionView.tag].comics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collcell", for: indexPath) as! CollectionViewCell
        
        cell.imageView.image = UIImage(named: data[collectionView.tag].comics[indexPath.row])
        cell.nameLabel.text = data[collectionView.tag].comics[indexPath.row]
        
        return cell
        
    }
    
}
