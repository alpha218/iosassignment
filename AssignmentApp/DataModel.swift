//
//  DataModel.swift
//  AssignmentApp
//
//  Created by Zolo on 27/01/23.
//

import Foundation

struct Comics {
    
    let category : String
    let comics : [String]
    
}
